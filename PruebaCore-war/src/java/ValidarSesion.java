/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import edu.udg.core.data.model.Access;
import javax.annotation.PostConstruct;
import edu.udg.core.portal.util.Utileria;
import edu.udg.core.remote.ejb.Service;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

@Named("ValidarSesion")
@ViewScoped
public class ValidarSesion implements Serializable {

    private static final long serialVersionUID = 6142022l;

    private Utileria util;

    private String nombre;

    @PostConstruct
    public void ValidarSesion() {

        this.nombre = "Hola mundo Noé";
        System.out.println("jkadhlaskdjalskdaljk");
        this.util = new Utileria();
        Boolean accesoAlmodulo;
        Access acceso = util.obtenerAccesoSesion();
        Service s = new Service();
        accesoAlmodulo = s.validaModulo(acceso.getToken(), acceso.getAppId(), util.obtenerModulo().replace("/", ""));
        if (!accesoAlmodulo) {
            util.redireccion("/error.xhtml");
        }

    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
